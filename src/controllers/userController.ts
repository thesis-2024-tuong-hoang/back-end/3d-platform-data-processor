import * as express from 'express'
// import { IUserRegister, IUserLogin, ITokenDecodedPayload } from '~/type'
import UserServices from '~/services/userServices'
// import TokenServices from '~/services/tokenServices'
// import { validationResult } from 'express-validator'
// import JWT from '~/utils/jwt'

class UserController {
    static async getUserById(req: express.Request, res: express.Response, next: express.NextFunction) {
        const body = req.body as any

        try {
            const response = await UserServices.getUserById(body.userId)
            console.log('Res: ', response)

            if (response) {
                res.status(200).json({
                    user: response,
                    status: 200
                })
            }
        } catch (error) {
            next(error)
        }
    }

    static async getUserBySlug(req: express.Request, res: express.Response, next: express.NextFunction) {
        const userSlug = req.params.userSlug as string

        console.log(userSlug)

        try {
            const user = await UserServices.getUserBySlug(userSlug)

            if (user) {
                res.status(200).json({
                    user: user,
                    status: 200
                })
            }
        } catch (error) {
            next(error)
        }
    }

    static async updateUserById(req: express.Request, res: express.Response, next: express.NextFunction) {
        const body = req.body as any

        try {
            const response = await UserServices.updateUserById(body)

            if (response) {
                res.status(response.status).json({
                    status: 204
                })
            }
        } catch (error) {
            next(error)
        }
    }
}

export default UserController
