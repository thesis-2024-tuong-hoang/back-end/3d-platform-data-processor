import * as express from 'express'
// import { IUserRegister, IUserLogin, ITokenDecodedPayload } from '~/type'
// import UserServices from '~/services/userServices'
import ModelServices from '~/services/modelServices'
// import { validationResult } from 'express-validator'
// import JWT from '~/utils/jwt'
import { IModelRow } from '~/services/modelServices'
import UserServices from '~/services/userServices'

export interface IModelBody {
    description: string
    name: string
    userId: string
}

class ModelController {
    static async uploadNewModelInfo(req: express.Request, res: express.Response, next: express.NextFunction) {
        const modelData = req.body as IModelBody

        const response = await ModelServices.uploadNewModelInfo(modelData)
        res.status(response.status).json({
            message: 'Upload a new model successfully'
        })
    }

    static async getAllModels(req: express.Request, res: express.Response, next: express.NextFunction) {
        const response = await ModelServices.getAllModels()
        // console.log(response)

        res.status(response.status).json({
            status: 200,
            models: response.models
        })
    }

    static async getAllModelsByUserId(req: express.Request, res: express.Response, next: express.NextFunction) {
        const userId = req.params.userId

        const response = await ModelServices.getAllModelsByUserId(userId)

        res.status(response.status).json({
            status: 200,
            models: response.models
        })
    }

    static async getModalByName(req: express.Request, res: express.Response, next: express.NextFunction) {
        const modelName = req.params.modelName as any

        const response = await ModelServices.getModelByName(modelName)
        res.status(response.status).json({
            user: response.user,
            model: response.model
        })
    }

    static async getModalsByUserId(req: express.Request, res: express.Response, next: express.NextFunction) {
        const userId = req.body.userId as string
        const modelResponse = await ModelServices.getModalsByUserId(userId)
        const userResponse = await UserServices.getUserById(userId)

        res.status(200).json({
            modelList: modelResponse.modelList,
            user: userResponse
        })
    }
}

export default ModelController
