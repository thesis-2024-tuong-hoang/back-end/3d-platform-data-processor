import { Request, Response, NextFunction } from 'express'

export default function errorHandler(error: any, req: Request, res: Response, next: NextFunction) {
    console.log('Error here: ', error)
    const status = error.status || 500
    const message = error.message
    res.status(status).json({ message: message, error: error })
}
