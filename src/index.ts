import express from 'express'
import logger from 'morgan'
import router from './routes'
import cookieParser from 'cookie-parser'
import errorHandler from './middlewares/error'

const app = express()

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())

app.use('/process-data', router)

app.use(errorHandler)

console.log('Data Processor running on localhost:3004')

app.listen(3004)
