import { Router } from 'express'
import ModelController from '~/controllers/modelController'
import UserController from '~/controllers/userController'

// import { checkSchema } from 'express-validator'
import ValidateSchemas from '~/utils/validate'

const router = Router()

// router.post('/register', checkSchema(ValidateSchemas.register, ['body']), AuthController.register)

// router.post('/login', checkSchema(ValidateSchemas.login, ['body']), AuthController.login)

router.post('/upload-model', ModelController.uploadNewModelInfo)

router.get('/3d-models/all', ModelController.getAllModels)

router.get('/3d-models/all/:userId', ModelController.getAllModelsByUserId)

router.post('/3d-models', ModelController.getModalsByUserId)

router.get('/3d-models/:modelName', ModelController.getModalByName)

router.get('/user/:userSlug', UserController.getUserBySlug)

router.post('/user', UserController.getUserById)

router.put('/user', UserController.updateUserById)

// router.post('/authorize', isAuthorized)

export default router
