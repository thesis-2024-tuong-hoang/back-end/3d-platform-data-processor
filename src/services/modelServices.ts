import supabase from '~/constants/supabaseConfig'
import { ErrorResponse } from '~/utils/errors'
// import { Result, ValidationError } from 'express-validator'
import { IUserInfo, IUserRegister, IUserLogin } from '~/type'
import { IModelBody } from '~/controllers/modelController'

export interface IModelRow {
    created_at?: string | undefined
    description?: string | null | undefined
    downloaded?: number | undefined
    file_name: string
    user_id: string
    id?: string | undefined
    liked?: number | undefined
    name?: string | undefined
    stars?: number | undefined
    viewed?: number | undefined
}

class ModelServices {
    static uploadNewModelInfo = async (modelObj: IModelBody) => {
        const modelRowData: IModelRow = {
            file_name: modelObj.name.toLowerCase().split(' ').join('-'),
            name: modelObj.name,
            description: modelObj.description,
            user_id: modelObj.userId
        }
        const response = await supabase.from('models').insert(modelRowData)
        return response
    }

    static getAllModels = async () => {
        const modelResponse = await supabase.from('models').select('*')

        const responseBody = {
            models: modelResponse.data,
            status: 200
        }

        return responseBody
    }
    static getAllModelsByUserId = async (userId: string) => {
        const modelResponse = await supabase.from('models').select('*').eq('user_id', userId)

        const responseBody = {
            models: modelResponse.data,
            status: 200
        }

        return responseBody
    }

    static getModelByName = async (modelName: string) => {
        const modelResponse = await supabase.from('models').select('*').eq('file_name', modelName)
        const userResponse = await supabase.from('users').select('*').eq('id', modelResponse.data![0].user_id)

        const responseBody = {
            model: {
                ...modelResponse.data![0]
            },
            user: {
                ...userResponse.data![0]
            },
            status: 200
        }

        return responseBody
    }

    static getModalsByUserId = async (userId: string) => {
        console.log(userId)
        const modelsResponse = await supabase.from('models').select('*').eq('user_id', userId)
        // const userResponse = await supabase.from('users').select('*').eq('id', modelResponse.data![0].user_id)

        console.log('Models: ', modelsResponse.data)

        const responseBody = {
            modelList: modelsResponse.data,
            status: 200
        }

        return responseBody
    }

    private static updateModelById = async (newUser: IUserInfo) => {
        // const { data: users } = await supabase.from('users').insert(newUser).select('id')
        // return users
    }
}

export default ModelServices
