import supabase from '~/constants/supabaseConfig'
import { ErrorResponse } from '~/utils/errors'
// import { Result, ValidationError } from 'express-validator'
import { IUserInfo, IUserRegister, IUserLogin } from '~/type'

class UserServices {
    static getUserById = async (id: string) => {
        const response = await supabase.from('users').select('*').eq('id', id)
        const user = response.data?.pop()
        return user
    }

    static getUserBySlug = async (slug: string) => {
        const response = await supabase.from('users').select('*').eq('slug', slug)
        const user = response.data?.pop()
        return user
    }

    static updateUserById = async (newData: any) => {
        const user = await this.getUserById(newData.id)
        const response = await supabase.from('users').update(newData).eq('id', newData.id)
        return response
    }
}

export default UserServices
